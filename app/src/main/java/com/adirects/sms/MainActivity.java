package com.adirects.sms;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.PermissionListener;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private Button btn_setting;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn_setting = (Button) findViewById(R.id.btn_setting);
        btn_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,EmailSettingActivity.class);
                startActivity(intent);
            }
        });

        AndPermission.with(this)
                .requestCode(200)
                .permission(
                         Manifest.permission.BROADCAST_SMS
                        ,Manifest.permission.RECEIVE_SMS
                        ,Manifest.permission.READ_SMS)
                .callback(listener)
                .start();

        Intent service = new Intent(this,NoticeService.class);
        startService(service);
    }

    private PermissionListener listener = new PermissionListener() {
        @Override
        public void onSucceed(int requestCode, List<String> grantedPermissions) {
            // Successfully.
            if(requestCode == 200) {
                // TODO ...
            }
        }

        @Override
        public void onFailed(int requestCode, List<String> deniedPermissions) {
            // Failure.
            if(requestCode == 200) {
                // TODO ...
            }
        }
    };
}
