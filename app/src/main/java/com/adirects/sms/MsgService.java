package com.adirects.sms;

import android.app.IntentService;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;

import com.adirects.sms.mail.MailInfo;
import com.adirects.sms.mail.MailSender;
import com.adirects.sms.util.PreferencesUtil;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 */
public class MsgService extends IntentService {

    public MsgService() {
        super("MsgService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            String sender = intent.getStringExtra("sender");
            String time = intent.getStringExtra("time");
            String content = intent.getStringExtra("content");
            Log.e("onHandleIntent", "sender:"+sender);
            Log.e("onHandleIntent", "time:"+time);
            Log.e("onHandleIntent", "content:"+content);

            String fromAdd = PreferencesUtil.getInstance(this).getString("fromAdd");
            String password = PreferencesUtil.getInstance(this).getString("password");
            String toAdd = PreferencesUtil.getInstance(this).getString("toAdd");
            if (!(TextUtils.isEmpty(fromAdd)||TextUtils.isEmpty(password)||TextUtils.isEmpty(toAdd))){
                MailSender mailSender = new MailSender();
                MailInfo mailInfo = new MailInfo();
                mailInfo.setFromAddress(fromAdd);
                mailInfo.setToAddress(toAdd);
                mailInfo.setMailServerHost("smtp."+fromAdd.split("@")[1]);
                mailInfo.setMailServerPort("25");
                mailInfo.setSubject("SMS");
                mailInfo.setContent("sender:"+sender+"\ntime:"+time+"\ncontent:"+content);
                mailInfo.setUserName(fromAdd);
                mailInfo.setPassword(password);
                boolean isSend = mailSender.sendTextMail(mailInfo);
                if (isSend){
                    System.out.println("邮件发送成功");
                }


            }

        }
    }



}
