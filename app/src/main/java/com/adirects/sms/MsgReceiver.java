package com.adirects.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MsgReceiver extends BroadcastReceiver {
    public static final String SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED";

    @Override
    public void onReceive(Context context, Intent intent) {
        Object[] pdus = (Object[]) intent.getExtras().get("pdus");
        if (pdus != null) {
            for (Object pdu : pdus) {

                SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdu);
                String sender = smsMessage.getDisplayOriginatingAddress();
                String content = smsMessage.getMessageBody();
                long date = smsMessage.getTimestampMillis();
                Date timeDate = new Date(date);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                String time = simpleDateFormat.format(timeDate);
                System.out.println("---------MsgReceiver ------------");
                System.out.println("短信来自:" + sender);
                System.out.println("短信内容:" + content);
                System.out.println("短信时间:" + time);
                System.out.println("-----------End-------------------");
//                Intent service = new Intent(context, MsgService.class);
//                service.putExtra("sender", sender);
//                service.putExtra("time", time);
//                service.putExtra("content", content);
//                context.startService(service);
            }

        }
    }
}
