package com.adirects.sms;

import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.adirects.sms.util.PreferencesUtil;

public class EmailSettingActivity extends AppCompatActivity implements View.OnClickListener {

    private Toolbar toolbar;
    private Button btn_submit;
    private TextInputEditText et_fromAdd;
    private TextInputEditText et_pass;
    private TextInputEditText et_toAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_setting);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        et_fromAdd = (TextInputEditText) findViewById(R.id.et_fromAdd);
        et_pass = (TextInputEditText) findViewById(R.id.et_pass);
        et_toAdd = (TextInputEditText) findViewById(R.id.et_toAdd);
        btn_submit = (Button) findViewById(R.id.btn_submit);
        btn_submit.setOnClickListener(this);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        String fromAdd = PreferencesUtil.getInstance(this).getString("fromAdd");
        String password = PreferencesUtil.getInstance(this).getString("password");
        String toAdd = PreferencesUtil.getInstance(this).getString("toAdd");
        if ((!TextUtils.isEmpty(fromAdd)) && (!TextUtils.isEmpty(password))) {
            et_fromAdd.setText(fromAdd);
            et_pass.setText(password);
        }

        if (!TextUtils.isEmpty(toAdd)) {
            et_toAdd.setText(toAdd);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_submit:
                String et_fromStr = et_fromAdd.getText().toString().trim();
                String et_passStr = et_pass.getText().toString().trim();
                String et_toStr = et_toAdd.getText().toString().trim();
                boolean sender = (!TextUtils.isEmpty(et_fromStr)) && (!TextUtils.isEmpty(et_passStr));
                boolean receiver = (!TextUtils.isEmpty(et_toStr));
                if (sender && receiver) {
                    PreferencesUtil.getInstance(this).add("fromAdd", et_fromAdd.getText().toString().trim());
                    PreferencesUtil.getInstance(this).add("password", et_pass.getText().toString().trim());
                    PreferencesUtil.getInstance(this).add("toAdd", et_toAdd.getText().toString().trim());
                    Toast.makeText(this,"设置成功",Toast.LENGTH_SHORT).show();
                    finish();
                }else {
                    Toast.makeText(this,"请完善邮箱信息",Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

}
