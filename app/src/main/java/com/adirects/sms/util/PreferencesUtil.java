package com.adirects.sms.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * @author Adirects
 */

public class PreferencesUtil {

    public static final String AD_DATA = "ad_data";
    private static String PREFER_NAME = "Adirects";//此处可以改为自己想设置的名称,即可完成个人喜好配置私有化
    private static int MODE = Context.MODE_PRIVATE;//
    private Context context;
    private static PreferencesUtil instance = null;

    private PreferencesUtil() {
    }

    private PreferencesUtil(Context context) {
        this.context = context.getApplicationContext();

    }

    public static PreferencesUtil getInstance(Context context) {
        if (instance == null) {
            synchronized (PreferencesUtil.class) {
                if (instance == null) {
                    instance = new PreferencesUtil(context);
                }
            }
        }
        return instance;
    }

    private SharedPreferences getSharePreferences() {
        if (PREFER_NAME == null) {
            return PreferenceManager.getDefaultSharedPreferences(context);
        } else {
            return context.getSharedPreferences(PREFER_NAME, MODE);
        }
    }


    public void setName(String preferName) {
        PREFER_NAME = preferName;
    }

    /**
     * 添加一条key-value 数据
     *
     * @param key   key
     * @param value value
     */
    public void add(final String key, final String value) {
        getSharePreferences().edit().putString(key, value).apply();
    }

    /**
     * 添加一条key-value 数据
     *
     * @param key   key
     * @param value value
     */
    public void add(String key, Boolean value) {
        getSharePreferences().edit().putBoolean(key, value).apply();
    }

    /**
     * 添加一条key-value 数据
     *
     * @param key   key
     * @param value value
     */
    public void add(String key, Float value) {
        getSharePreferences().edit().putFloat(key, value).apply();
    }

    /**
     * 添加一条key-value 数据
     *
     * @param key   key
     * @param value value
     */
    public void add(String key, int value) {
        getSharePreferences().edit().putInt(key, value).apply();
    }

    /**
     * 添加一条key-value 数据
     *
     * @param key   key
     * @param value value
     */
    public void add(String key, long value) {
        getSharePreferences().edit().putLong(key, value).apply();
    }

    /**
     *添加一条key-value 数据
     * @param key  key
     * @param value value  value必须继承Serializable 序列化
     *
     * uid: private static final long serialVersionUID = 8683452581122892189L;
     */
//    public static void add(String key ,Object value){
//        add(key, encrypt(StringUtil.OtoString(value)));
//    }

    /**
     * 获取配置文件
     *
     * @param key 根据key获取数据
     * @return 字符串  没有则返回null
     */
    public String getString(String key) {
        return getSharePreferences().getString(key, null);
    }

    /**
     * 获取配置文件
     *
     * @param key 根据key获取数据
     * @return 字符串  没有则返回defValue
     */
    public String getString(String key, String defValue) {
        return getSharePreferences().getString(key, defValue);
    }

    /**
     * 获取Boolean 值，
     *
     * @param key key
     * @return value 没有则返回false
     */
    public boolean getBoolean(String key) {
        return getSharePreferences().getBoolean(key, false);
    }

    /**
     * 获取Boolean 值，
     *
     * @param key key
     * @return value 没有则返回defValue
     */
    public boolean getBoolean(String key, boolean defValue) {
        return getSharePreferences().getBoolean(key, defValue);
    }

    /**
     * 获取Float
     *
     * @param key key
     * @return value 没有则返回0
     */
    public float getFloat(String key) {
        return getSharePreferences().getFloat(key, 0);
    }

    /**
     * 获取Float
     *
     * @param key key
     * @return value 没有则返回defValue
     */
    public float getFloat(String key, float defValue) {
        return getSharePreferences().getFloat(key, defValue);
    }

    /**
     * 获取Int值
     *
     * @param key key
     * @return value 没有则返回0
     */
    public int getInt(String key) {
        return getSharePreferences().getInt(key, 0);
    }

    /**
     * 获取Int值
     *
     * @param key key
     * @return value 没有则返回defValue
     */
    public int getInt(String key, int defValue) {
        return getSharePreferences().getInt(key, defValue);
    }

    /**
     * 获取Long值
     *
     * @param key key
     * @return value 没有则返回0
     */
    public long getLong(String key) {
        return getSharePreferences().getLong(key, 0);
    }

    /**
     * 获取Long值
     *
     * @param key key
     * @return value 没有则返回defValue
     */
    public long getLong(String key, long defValue) {
        return getSharePreferences().getLong(key, defValue);
    }

    /**
     * 获取Object对象，
     * @param key key
     * @return value 没有则返回null
     */
//    public  Object getObject(String key){
//        String value = getString(key);
//        if(value==null) return null;
//        return StringUtil.stringToO(decrypt(value));
//    }

    /**
     * 清空配置文件所有数据
     *
     * @param p
     */
    public void clear(SharedPreferences p) {
        SharedPreferences.Editor editor = p.edit();
        editor.clear();
        editor.apply();
    }

    /**
     * 移除相关key的数据
     *
     * @param key
     */
    public void remove(String key) {
        getSharePreferences().edit().remove(key).apply();
    }

//    private static final String key="k*UOlPaq091IU)^brm910*@23EW42ast";//32bit
//    private static final String iv="Yn230*pkdj)64qom";//16bit

    /**
     * 加密函数
     * @param str
     * @return
     */
//    public static String encrypt(String str){
//
//        try {
//            return new AesCryptUtil().encrypt(str,key,iv);
//        } catch (InvalidKeyException e) {
//            e.printStackTrace();
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        } catch (InvalidAlgorithmParameterException e) {
//            e.printStackTrace();
//        } catch (IllegalBlockSizeException e) {
//            e.printStackTrace();
//        } catch (BadPaddingException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (NoSuchPaddingException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }

    /**
     * 解密函数
     * @param string
     * @return
     */
//    public static String decrypt(String string){
//        try {
//            return new AesCryptUtil().decrypt(string,key,iv);
//        } catch (InvalidKeyException e) {
//            e.printStackTrace();
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        } catch (InvalidAlgorithmParameterException e) {
//            e.printStackTrace();
//        } catch (IllegalBlockSizeException e) {
//            e.printStackTrace();
//        } catch (BadPaddingException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (NoSuchPaddingException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
}
