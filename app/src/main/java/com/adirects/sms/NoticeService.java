package com.adirects.sms;

import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsMessage;
import android.text.TextUtils;

import com.adirects.sms.mail.MailInfo;
import com.adirects.sms.mail.MailSender;
import com.adirects.sms.util.PreferencesUtil;

import java.text.SimpleDateFormat;
import java.util.Date;


public class NoticeService extends Service {
    public static final String SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED";
    private static final int EMAIL_FAILD = 0;
    private static final int EMAIL_SUCCES = 1;
    private SmsReceiver smsReceiver;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == EMAIL_FAILD){
                builder.setContentText("邮件发送失败");
                notificationManager.notify(notifyId,builder.build());

            }else if (msg.what == EMAIL_SUCCES){
                builder.setContentText("邮件发送成功");
                notificationManager.notify(notifyId,builder.build());
            }
        }
    };
    private NotificationCompat.Builder builder;
    private NotificationManager notificationManager;
    private final int notifyId = 1;

    @Override
    public void onCreate() {

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        builder = new NotificationCompat.Builder(this);
        builder.setSmallIcon(R.mipmap.ic_launcher)
                .setWhen(System.currentTimeMillis());
//        builder.setTicker("Foreground Service Start");
//        builder.setContentTitle("Foreground Service");
//        builder.setContentText("Make this service run in the foreground.");
//        Notification notification = builder.build();
        startForeground(notifyId, builder.build());
        smsReceiver = new SmsReceiver();
        IntentFilter filter = new IntentFilter(SMS_RECEIVED_ACTION);
        filter.setPriority(Integer.MAX_VALUE);
        registerReceiver(smsReceiver, filter);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        if (smsReceiver != null) {
            unregisterReceiver(smsReceiver);
        }
        handler.removeCallbacksAndMessages(null);
    }

    private class SmsReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(SMS_RECEIVED_ACTION)) {

                Object[] pdus = (Object[]) intent.getExtras().get("pdus");
                if (pdus != null) {
                    for (Object pdu : pdus) {
                        SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdu);
                        String sender = smsMessage.getDisplayOriginatingAddress();
                        String content = smsMessage.getMessageBody();
                        long date = smsMessage.getTimestampMillis();
                        Date timeDate = new Date(date);
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        String time = simpleDateFormat.format(timeDate);
                        System.out.println("-----------NoticeService---------------");
                        System.out.println("短信来自:" + sender);
                        System.out.println("短信内容:" + content);
                        System.out.println("短信时间:" + time);
                        System.out.println("-----------NoticeService  End----------");
//                        Intent service = new Intent(context, MsgService.class);
//                        service.putExtra("sender", sender);
//                        service.putExtra("time", time);
//                        service.putExtra("content", content);
//                        context.startService(service);
                        sendEmail(context, sender, time, content);
                    }

                }
            }
        }
    }

    private void sendEmail(final Context context, final String sender, final String time, final String content) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String fromAdd = PreferencesUtil.getInstance(context).getString("fromAdd");
                String password = PreferencesUtil.getInstance(context).getString("password");
                String toAdd = PreferencesUtil.getInstance(context).getString("toAdd");
                if (!(TextUtils.isEmpty(fromAdd) || TextUtils.isEmpty(password) || TextUtils.isEmpty(toAdd))) {
                    MailSender mailSender = new MailSender();
                    MailInfo mailInfo = new MailInfo();
                    mailInfo.setFromAddress(fromAdd);
                    mailInfo.setToAddress(toAdd);
                    mailInfo.setMailServerHost("smtp." + fromAdd.split("@")[1]);
                    mailInfo.setMailServerPort("25");
                    mailInfo.setSubject("SMS");
                    mailInfo.setContent("sender:" + sender + "\ntime:" + time + "\ncontent:" + content);
                    mailInfo.setUserName(fromAdd);
                    mailInfo.setPassword(password);
                    boolean isSend = mailSender.sendTextMail(mailInfo);
                    if (isSend) {
                        System.out.println("邮件发送成功");
                        handler.sendEmptyMessage(EMAIL_SUCCES);
                    }else {
                        handler.sendEmptyMessage(EMAIL_FAILD);
                    }
                }
            }
        }).start();
    }
}
