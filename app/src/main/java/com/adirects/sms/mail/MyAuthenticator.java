package com.adirects.sms.mail;

import android.text.TextUtils;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;

/**
 * @author xuefeng
 */

public class MyAuthenticator extends Authenticator {
    private String username;
    private String password;

    public MyAuthenticator(String username, String password) {
        super();
        this.username = username;
        this.password = password;
    }

    @Override
    public PasswordAuthentication getPasswordAuthentication() {
        if (!TextUtils.isEmpty(username) && (!TextUtils.isEmpty(password))) {
            return new PasswordAuthentication(username, password);
        }
        return null;
    }

}
